const WebpackDevServer = require('webpack-dev-server')
const webpack = require('webpack')
const ngrok = require ('ngrok');
const config = require('./webpack.config.js')
var path = require('path');

console.log("Starting server");

new WebpackDevServer(webpack(
  config
),{
  hot: true,
  inline: true,
  compress: true,
  disableHostCheck:true,
  publicPath: config.output.publicPath,
  contentBase:'.',
  filename: config.output.filename,
  // historyApiFallback: config.devServer.historyApiFallback,
  stats: 'minimal',
  historyApiFallback: true,
  noInfo: true,
  host: '0.0.0.0',
  port: 9090,
  open:true,
  headers: { "X-Custom-Header": "yes" }
  // headers: {'Access-Control-Allow-Origin': '*'},


  // https: {
  //   cert: fs.readFileSync("path-to-cert-file.pem"),
  //   key: fs.readFileSync("path-to-key-file.pem"),
  //   cacert: fs.readFileSync("path-to-cacert-file.pem")
  // }
}
).listen(9090, 'localhost',(err) => {
      if (err){
          console.log(err);
          console.log("hola");

      }else{
            ngrok.connect({addr:8080,proto:'http',bind_tls:false},(innerErr, url ) => {
              if (innerErr){
                console.log('Error\n' + innerErr);
              }
              console.log('Tunnel init');
              console.log('Tunnel url: ' + url);
            })
      }

})
