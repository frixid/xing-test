const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

var path = require('path');

const cssLoaders = ['css-loader', 'postcss-loader'];

module.exports = {
    resolve: {
        extensions: ['*', '.jsx', '.js', '.css']
    },
    // context: path.join(__dirname, 'app'),
    entry: [
      path.join(__dirname, 'app/app.jsx'),
    ],
    optimization: {
    minimizer: [
      // we specify a custom UglifyJsPlugin here to get source maps in production
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true
        },
        sourceMap: true,
        // output: {
        //   comments: false,
        // },
         exclude: [/\.min\.js$/gi] // skip pre-minified libs
      })
    ]
  },


  //
  // compress: {
  //   warnings: false, // Suppress uglification warnings
  //   pure_getters: true,
  //   unsafe: true,
  //   unsafe_comps: true,
  //   screw_ie8: true
  // },
  //

    module: {
        rules: [

            {
            test: /\.css$/,
       //      use: ExtractTextPlugin.extract({
       //   fallback: "style-loader",
       //   use: "css-loader"
       // })
            use: [
              // {loader: MiniCssExtractPlugin.loader},
              // {loader: "style-loader"},
              {loader: "css-loader",
               options:{
                 modules:true,
                 importLoaders:1,
                 localIdentName:"[name]_[local]_[hash:base64]",
                 sourceMap:false,
                 minimize:true
               }
               }]


            // use: ExtractTextPlugin.extract({
            //     fallback: "style-loader",
            //     use:"css-loader"
            //     // use: [{
            //     //     loader: "css-loader",
            //     //     options: {
            //     //         modules: true,
            //     //         localIdentName: "[local]--[hash:base64:5]",
            //     //         importLoaders: true
            //     //     }
            //     // }]
            // })
        },

            {
                test: /(\.js|\.jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'

            },
            {
      test: /\.(otf|eot|woff|woff2|ttf|gif|svg|jpg|png)$/,
      loader: "file-loader",
    }

        ],
    },
    output: {
        path: path.join(__dirname,'assets/bundle/'),
        publicPath: "/assets/bundle/",
        filename: 'bundle.js'
    }
    ,
    plugins:[
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/,/moment-timezone$/),
//       new webpack.NormalModuleReplacementPlugin(
//   /moment-timezone\/data\/packed\/latest\.json/,
//   require.resolve('./misc/timezone-definitions')
// )
      // new ExtractTextPlugin("styles.css"),
      new webpack.optimize.AggressiveMergingPlugin(),
      new CompressionPlugin({asset: "[path].gz[query]", algorithm: "gzip", test: /\.js$|\.css$|\.html$/, threshold: 10240, minRatio: 0.8}), //production
      new webpack.DefinePlugin(
        {
          'process.env': {
              'NODE_ENV': JSON.stringify('production')
          }
      }),
      new HtmlWebpackPlugin({template: 'app/index.html', inject: 'body', filename: 'index.html'}),
    ]

};
