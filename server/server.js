'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Lout = require('lout');
const Vision = require('vision');
const Good = require('good');
const GoodConsole = require('good-console');

const server = new Hapi.Server();
const port = 7070;
const host = '0.0.0.0';

server.connection({
    port: port,
    host: host,
    routes: { cors: true }
});

const loutRegister = {
    register: Lout,
    options: { endpoint: '/docs' }
};

const monitor = {
    register: Good,
    options: {
        ops: {
            interval: 2000
        },
        reporters: {
            myConsoleReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ log: '*', response: '*' }]
            }, {
                module: 'good-console'
            }, 'stdout'],
            myFileReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ ops: '*' }]
            }, {
                module: 'good-squeeze',
                name: 'SafeJson'
            }, {
                module: 'good-file',
                args: ['./logs/awesome_log']
            }],
            myHTTPReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ error: '*' }]
            }, {
                module: 'good-http',
                args: ['http://prod.logs:3000', {
                    wreck: {
                        headers: { 'x-api-key': 12345 }
                    }
                }]
            }]
        }
    }

}



// ---- Default -----
// Load plugins and start server

server.register(
    [
        monitor, Vision, Inert, loutRegister, require('./App/routes')
    ]

    , error => {
        if (error) {
            return console.error(error);
        }
        // Start the server
        server.start(() => console.log('Server running at:', server.info.uri));
    })
