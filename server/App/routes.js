"use strict";

const Boom = require("boom");
const uuid = require("node-uuid");
const Joi = require("joi");
const bcrypt = require("bcrypt");
const fetch = require("node-fetch");
const util = require("util");
var OAuth = require("oauth");

exports.register = function(server, options, next) {
  server.route({
    method: "GET",
    path: "/locations",
    handler: function(request, reply) {
      fetch("http://gd.geobytes.com/AutoCompleteCity?&filter=DE&q="+ request.query.key)
        .then(response => response.json())
        .then(json => {
          return reply(json);
        })
     }
  });
  return next();
};

exports.register.attributes = {
  name: "routes-users"
};
