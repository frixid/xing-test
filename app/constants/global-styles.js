import { createGlobalStyle } from "styled-components";
import Background from "../../assets/images/background.png";

const GlobalStyle = createGlobalStyle`

::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: gray;
    opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: red;
}

::-ms-input-placeholder { /* Microsoft Edge */
    color: red;
}

  html,
  body{
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  #app{
    margin: 0 auto;
    min-width: 320px;
    max-width: 1024px;
  }
  a{text-decoration: none;}
  img {
      max-width: 100%;
      height: auto;
  }
 p,
 label{
   line-height: 1.5em;
 }


 li {
   position: inherit;
    list-style-type: none;
  }


span{
      -webkit-font-smoothing: antialiased;
}

  .title{
    color: white;
    position: sticky;
    padding: 20px 40px;
  }

.inputSearch{
    float:left
}
.enterKeyword input{
  width: 265px;
}
.Search{
  position: sticky;
  display: flex;
  padding: 40px;
}
.content{
  background: url(${Background}) center center no-repeat;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  width: 100%;
  height: 500px;
  background-size: cover;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-image: linear-gradient(to right,#1f7169 61%,#7a9c3b);
    opacity: .6;
    }
}

`;

export default GlobalStyle;
