import styled from "styled-components";

const StyledCategories = styled.div`
  float: left;
  margin-right: 20px;
  width: 512px;
  .categoriesSearch {
    border-radius: 4px;
    box-shadow: 1px 0px 0px 4px #00000024;
  }

  .enterKeyword {
    float: left;
    border-radius: 4px;
  }

  .enterKeyword input {
    width: 265px;
  }
  .selector {
    background: #e6e6e6;
    text-indent: 10px;
    padding: 8px 0;
    border-radius: 4px;
    user-select: none;
  }
  .selector:hover {
    cursor: pointer;
  }
  .selector input {
    background: #e0e0e0;
  }
`;

export default StyledCategories;
