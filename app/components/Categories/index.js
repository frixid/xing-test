import React, { Component, Fragment } from "react";
import { bindActionCreators } from "redux";

import Input from "../../components/Input";
import Form from '../../components/Form';
import SearchBtn from '../../components/SearchBtn';
import { connect } from "react-redux";
import * as actions from "../../containers/Home/actions";

import Checkbox from "../../components/Checkbox";
import ModalCategories from "../../components/ModalCategories"
import StyledCategories from "./styles.js"

class Categories extends Component {
  render() {
    const { allCategories,modalCategories,category,checkboxes } = this.props;

    return (
      <StyledCategories>
      <Form onSubmit={this.props.actions.onSubmitForm}>
        <div className="categoriesSearch">
          <div className="enterKeyword">
            <Input allCategories={allCategories} name={""} placeholder={"Enter keyword"} />
          </div>
          <div onClick={()=> this.props.actions.showmodalCategories()} className="selector">
            {category}
          </div>
        </div>
        {modalCategories
          && <ModalCategories checkboxes={checkboxes}
          allCategories={allCategories}
          actions={this.props.actions} />
        }
        </Form>
      </StyledCategories>
    );
  }
}
const mapStateToProps = state => ({
  checkboxes: state.homeReducer.checkboxes,
  allCategories:state.homeReducer.allCategories,
  category:state.homeReducer.category,
  modalCategories:state.homeReducer.modalCategories,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);
