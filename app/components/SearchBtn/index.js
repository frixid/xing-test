import React, { Component, Fragment } from "react";
import StyledButton from "./styles.js";

class SearchBtn extends Component {
  render() {
    const { name } = this.props;
    return (
      <>
        <StyledButton> {name}</StyledButton>
      </>
    );
  }
}

export default SearchBtn;
