import styled from "styled-components";

const StyledButton = styled.button`
  font-weight: 700;
  font-size: 14px;
  border-radius: 3px;
  border: 0;
  padding: 8px 18px;
  margin-left: 16px;
  background: #b0d400;
  background-image: linear-gradient(to bottom, #bde300 0, #b0d400 100%);
`;
export default StyledButton;
