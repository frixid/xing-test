import styled from "styled-components";

const Suggestion = styled.div`
position: relative;
.suggestion{
  background:white;
  padding: 3px 0px;
  padding-left: 7px;
  font-size: 12px;
  border: 1px solid #8080805c;
}

.active{
  color:yellow;
  background:#4a901ade
}

`;

export default Suggestion;
