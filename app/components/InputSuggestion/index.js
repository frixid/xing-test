import React, { Component, Fragment } from "react";

import InputSearch from "../../components/InputSearch"
import Suggestion from "./styles.js";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../containers/Home/actions";

class InputSuggestion extends Component {

  render() {
    const {
      placeholder,
      locations,
      position,
      userInput,
      selectedSearch,
      selected,
      actions
    } = this.props;

    const suggestions = [];

    if (locations.length !== 0 && locations[0] !== "%s") {
      locations.map((item, i) => {
        suggestions.push(
          <div
            className={position === i ? "suggestion active" : "suggestion"}
            key={i}
            onClick={() => this.props.actions.search(item)}
          >
            {item}
          </div>
        );
      });
    }

    return (
      <div className="inputSearch">
        <InputSearch
          placeholder={placeholder}
          position={position}
          actions={actions}
          userInput={userInput}
          selectedSearch={selectedSearch}
          locations={locations}
        />
      <Suggestion>{suggestions}</Suggestion>

    </div>
    );
  }
}


const mapStateToProps = state => ({
  placeholder:state.homeReducer.placeholder,
  locations: state.homeReducer.locations,
  position: state.homeReducer.position,
  userInput: state.homeReducer.userInput,
  selected: state.homeReducer.selected,
  selectedSearch: state.homeReducer.selectedSearch
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InputSuggestion);
