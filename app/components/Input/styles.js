import styled from "styled-components";

const StyledInput = styled.input`
    color: gray;
    position: inherit;
    border-radius: 4px 0px 0px 4px;
    font-size: 15px;
    text-indent: 10px;
    border: 0px solid transparent;
    padding: 8px 0;
    text-align: left;
    font-weight: lighter;
  &:focus {
    outline: 0;
  }
`;

export default StyledInput;
