import React, { Component, Fragment } from "react";
import StyledInput from "./styles.js";

class Input extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {placeholder} = this.props
    return (
      <div>
        <StyledInput
          placeholder={placeholder}
          type={"type"}
          />
      </div>
    );
  }
}
export default Input;
