import React,{Component,Fragment} from 'react'
import StyledInputSearch from "./styles.js"

class InputSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      ENTER_KEY: 13,
      UP_KEY: 38,
      DOWN_KEY: 40,
      POSITION: 1
    };
  }
  handleChange = e => {
    this.triggerChange(e.target.value);
  };
  handleKeyDown = e => {
    switch (e.keyCode) {
      case this.state.ENTER_KEY:
        return this.props.actions.search(
          this.props.locations[this.props.position]
        );

      case this.state.UP_KEY:
        return this.props.actions.setKeyup(
          this.props.locations[this.props.position]
        );

      case this.state.DOWN_KEY:
        return this.props.actions.setKeydown(this.props.locations[this.props.position + 1]
        );

      default:
        return;
    }
  };
  triggerChange = value => {
    this.props.actions.getLocation(value);
  };
  render () {
    const {selectedSearch, userInput, placeholder} = this.props;

    return(
      <>
      <StyledInputSearch
        type={"type"}
        key={selectedSearch}
        placeholder={selectedSearch ? selectedSearch : placeholder }
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
      />

    </>
    )
  }
}

export default InputSearch;
