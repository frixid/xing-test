import React, {Component,Fragment} from "react";
import StyledCheckbox from "./styles.js"

class Checkbox extends Component {
  constructor(props){
    super(props)
  }
  handleChange = (e) => {
    this.props.actions.selectCheckbox(e.target.value)
  }
  render(){
    const {name,id,checked} = this.props;
    return(
        <StyledCheckbox >
          <input
            type="checkbox"
            onChange={this.handleChange}
            checked={checked}
            value={id} />
            <label>
                <span> {name}</span>
            </label>
        </StyledCheckbox>
    )
  }
}

export default Checkbox;
