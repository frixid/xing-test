import styled from "styled-components"

const StyledCheckbox = styled.div`
border: 0;
padding: 0px 12px;
input{
  margin: 6px;
  float:left
}
label{
  margin-left: 21px;
  display: block;
}
`
export default StyledCheckbox
