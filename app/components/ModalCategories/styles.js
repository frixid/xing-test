import styled from "styled-components";

const StyledModalCategories = styled.div`
  background: white;
  border-radius: 4px;
  margin-top: 4px;
  box-shadow: 1px 0px 0px 4px #00000024;
  .headerTop,
  .headerMore {
    height: 22px;
    padding: 5px 7px;
    border-top: 1px solid #8080805c;
    border-bottom: 1px solid #8080805c;
    font-weight: bold;
  }
  .topCategories {
    float: left;
  }
  .allCategories {
    float: right;
    font-weight: normal;
    color: teal;
    cursor: pointer;
  }
  .checkbox_lists {
    column-count: 2;
  }
  .allCategories {
    user-select: none;
  }
  .scrollMore {
    height: 100px;
    overflow-y: scroll;
  }
`;
export default StyledModalCategories;
