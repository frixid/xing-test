import React, { Component } from "react";
import Input from "../../components/Input";
import Checkbox from "../../components/Checkbox";
import StyledModalCategories from "./styles.js"

class ModalCategories extends Component {
  onCheck(e, checked) {
    this.props.input.onChange(checked);
  }
  render() {
    const { allCategories, checkboxes, actions } = this.props;
    const topCategoriesList = [];
    const moreCategoriesList = [];
    checkboxes.map((item, i) => {
      if (item.class == "top categories")
        topCategoriesList.push(
          <Checkbox
            key={i}
            actions={actions}
            id={item.id}
            name={item.name}
            checked={item.checked}
          />
        );
      else {
        moreCategoriesList.push(
          <Checkbox
            key={i}
            actions={actions}
            id={item.id}
            name={item.name}
            checked={item.checked}
          />
        );
      }
    });
    return (
      <StyledModalCategories>
        <div className="headerTop">
          <span className="topCategories">Top categories</span>
          <div
            onClick={() => this.props.actions.allCategoriesSelect()}
            className="allCategories"
          >
            {"Search in all categories"}
          </div>
        </div>
        <div className="checkbox_lists">{topCategoriesList}</div>
        <div className="headerMore">
          <span className="moreCategories">More Categories</span>
        </div>
        <div className="scrollMore">
          <div className="checkbox_lists">{moreCategoriesList}</div>
        </div>
      </StyledModalCategories>
    );
  }
}
export default ModalCategories;
