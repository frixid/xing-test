import React,{Fragment} from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route, Switch } from 'react-router' // react-router v4
import { ConnectedRouter } from 'connected-react-router'
import App from "./containers/Home/home";
import configureStore, { history } from './configureStore'
const store = configureStore();
if (module.hot) module.hot.accept();

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <>
        <Route exact path="/" component={App} />
      </>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("app")
);
