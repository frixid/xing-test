import { createBrowserHistory } from "history";
import { applyMiddleware, compose, createStore, combineReducers } from "redux";
import { routerMiddleware, connectRouter } from "connected-react-router";
import reducers from "./constants/reducers";
import thunk from "redux-thunk";
export const history = createBrowserHistory();
export default function configureStore(preloadedState) {
  const store = createStore(
    combineReducers({
      router: connectRouter(history),
      ...reducers
    }),
    preloadedState,
    compose(applyMiddleware(routerMiddleware(history), thunk))
  );
  return store;
}
