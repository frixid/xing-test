"use strict";
import "whatwg-fetch";

const host = "http://localhost:7070";

function request(method, endpoint, params, body, hostname) {
  var endpointURL = hostname + endpoint + params;
  if (!method || !endpointURL) return reject("Missing params (API.request).");
  try {
    return sendRequest(method, endpointURL, body);
  } catch (error) {
    throw error;
  }
}

function sendRequest(method, endpointUrl, body) {
  const req = {
    method: method.toUpperCase(),
    headers: {
      "Content-Type": "application/json"
    }
  };
  if (body) req.body = JSON.stringify(body);
  return fetch(endpointUrl, req)
    .then(checkStatus)
    .then(extractJson);
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.status);
  error.response = response;
  throw error;
}

const extractJson = response => response.json();
const API = {
  getLocation: key =>
    request("GET", "/locations", "?key=" + encodeURIComponent(key), "", host)
};

export default API;
