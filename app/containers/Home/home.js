import React, { Component, Fragment } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../containers/Home/actions";
import Categories from "../../components/Categories";
import InputSuggestion from "../../components/InputSuggestion";
import SearchBtn from "../../components/SearchBtn";
import GlobalStyle from "../../constants/global-styles";
import styled from "styled-components";
class Home extends Component {
  render() {
    return (
      <>
        <GlobalStyle />
        <div className="content">
          <div className="title">
            <span>
              <h1>
                For a better working life <br /> The new XING Jobs
              </h1>
            </span>
          </div>
          <div className="Search">
            <Categories />
            <div className="location">
              <InputSuggestion />
              <SearchBtn name={"Search"} />
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default Home;
