"use strict";

import ActionTypes from "../../constants/actionTypes";
import API from "../../lib/api";

export const onSubmitForm = value => {
  return dispatch => {
    dispatch({
      type: ActionTypes.SELECT_CHECKBOXC,
      payload: {
        checkbox: value
      }
    });
  };
};
export const selectCheckbox = value => {
  return dispatch => {
    dispatch({
      type: ActionTypes.SELECT_CHECKBOXC,
      payload: {
        id: value
      }
    });
  };
};
export const showmodalCategories = () => {
  return dispatch => {
    dispatch({
      type: ActionTypes.TOGGLE_MODALCATEGORIES
    });
  };
};
export const allCategoriesSelect = () => {
  return dispatch => {
    dispatch({
      type: ActionTypes.ALL_CATEGORIES
    });
  };
};

export const getLocation = query => {
  console.log(query);
  return dispatch => {
    API.getLocation(query)
      .then(jsonResponse => dispatch(places(jsonResponse, query)))
      .catch(error => {
        console.log("Not found" + error.message);
      });
  };
};
export const search = value => {
  console.log(value);
  return dispatch => {
    dispatch({
      type: ActionTypes.SEARCH,
      payload: {
        selected: value
      }
    });
  };
};
export const setKeyup = value => {
  return dispatch => {
    dispatch({
      type: ActionTypes.SET_KEYUP,
      payload: {
        selected: value
      }
    });
  };
};

export const setKeydown = value => {
  return dispatch => {
    dispatch({
      type: ActionTypes.SET_KEYDOWN,
      payload: {
        selected: value
      }
    });
  };
};

function places(jsonResponse, query) {
  return {
    type: ActionTypes.REQUEST_LOCATIONS,
    payload: {
      locations: jsonResponse,
      position: 0,
      userInput: query
    }
  };
}
