"use strict";
import ActionTypes from "../../constants/actionTypes";

const initialState = {
  modalCategories: false,
  locations: [],
  position: 0,
  placeholder: "Location",
  selected: "",
  selectedSearch: "",
  category: "in all categories",
  allCategories: true,
  checkboxes: [
    {
      id: 0,
      name: "IT and telecomunication (4440)",
      checked: true,
      class: "top categories"
    },
    {
      id: 1,
      name: "Sales and commerce (3262)",
      checked: true,
      class: "top categories"
    },
    {
      id: 2,
      name: "Production, construction and trade (2586)",
      checked: true,
      class: "top categories"
    },
    {
      id: 3,
      name: "Management / executive and strategic management (1982)",
      checked: true,
      class: "top categories"
    },
    { id: 4, name: "other (3636)", checked: true, class: "top categories" },
    {
      id: 5,
      name: "Engineering /technical (2652)",
      checked: true,
      class: "top categories"
    },
    {
      id: 6,
      name: "Health, medical and social (2130)",
      checked: true,
      class: "top categories"
    },
    {
      id: 7,
      name: "Finance and accounting (1979)",
      checked: true,
      class: "top categories"
    },
    {
      id: 8,
      name: "Not categorized (3636)",
      checked: true,
      class: "more categories"
    },
    {
      id: 9,
      name: "Banking, insurance and financial services (3636)",
      checked: true,
      class: "more categories"
    },
    {
      id: 10,
      name: "Purchasing, transport and logistics (3636)",
      checked: true,
      class: "more categories"
    },
    {
      id: 11,
      name: "Administration (3636)",
      checked: true,
      class: "more categories"
    },
    {
      id: 12,
      name: "Marketing and advertising (3636)",
      checked: true,
      class: "more categories"
    },
    {
      id: 13,
      name: "Training / instruction (3636)",
      checked: true,
      class: "more categories"
    },
    { id: 14, name: "others", checked: true, class: "more categories" },
    { id: 15, name: "others", checked: true, class: "more categories" },
    { id: 16, name: "others", checked: true, class: "more categories" },
    { id: 17, name: "others", checked: true, class: "more categories" }
  ]
};

export default function homeReducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SELECT_CHECKBOXC:
      return {
        ...state,
        checkboxes: state.checkboxes.map((item, index) => {
          if (item.id == action.payload.id) {
            return { ...item, checked: !item.checked };
          }
          return item;
        })
      };
    case ActionTypes.TOGGLE_MODALCATEGORIES:
      return {
        ...state,
        modalCategories: !state.modalCategories
      };
    case ActionTypes.ALL_CATEGORIES:
      return {
        ...state,
        checkboxes: state.checkboxes.map(item => {
          return { ...item, checked: true };
        }),
        allCategories: false
      };

    case ActionTypes.REQUEST_LOCATIONS:
      return {
        ...state,
        locations: [...action.payload.locations],
        placeholder: action.payload.userInput,
        position: 0
      };
    case ActionTypes.SEARCH:
      console.log(action.payload.selected);
      return {
        ...state,
        selected: action.payload.selected,
        selectedSearch: action.payload.selected
      };
    case ActionTypes.SET_KEYUP:
      return {
        ...state,
        selected: action.payload.selected,
        position: state.position !== 0 ? state.position - 1 : state.position
      };
    case ActionTypes.SET_KEYDOWN:
      return {
        ...state,
        selected: action.payload.selected,
        position:
          state.position !== state.locations.length - 1
            ? state.position + 1
            : state.position
      };
    default:
      return state;
  }
}
