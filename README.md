

Webpack: 4.5.0
webpack-dev-server: 3.1.4
Node: 8.11.2
Npm: 6.1.0

#XING test

## Test case

1. check XING-FE-test.pdf

## Quick Start


### Dependencies

1. Run '$ npm installDeps ' to install Frontend and Backend dependencies

### Frontend(react)

1. Run '$ npm start' to run webpack server

### Backend(node)

1. Run '$ cd server' and then '$ npm start' to run nodemon
